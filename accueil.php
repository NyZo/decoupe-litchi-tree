<?php $namePage="Home"; $lang ="fr"; ?>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Accueil - The Litchi Tree</title>
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<![endif]-->

<?php include "css/css.php";?>
<?php include "css/animate.php";?>
</head>

<body>
    <div id="home">
        <div class="bg"></div>
        <?php include "header.php";?>
        <div class="langue wow fadeInLeft">
            <ul>
                <li><a href="index.php">EN</a></li>
            </ul>
        </div>
        <div>
            <div class="wow fadeInDown" >
                <a href="accueil.php" title="The Litchi Tree"><img src="images/logo-home.png" alt="The Litchi Tree"></a>
                <div class="titrePage"><span>Madagascar</span> Montagne d'ambre</div>
            </div>
            <a href="page-decouvrir.php" title="Découvrir" class="btn wow fadeInUp" data-wow-delay="1s">
                <span>Découvrir</span>
            </a>
            <a href="page-reserver.php" title="Réserver" class="btn wow fadeInUp" data-wow-delay="1.5s">
                <span>Réserver</span>
            </a>
            <a href="page-explorer.php" title="Explorer" class="btn wow fadeInUp" data-wow-delay="2s">
                <span>Explorer</span>
            </a>
        </div>
    </div>
    <div class="blc-made show">
        <a href="http://maki-agency.com/" title="Made by Maki Agency" target="_blank"> <span><strong>Made by</strong> Maki Agency</span></a>
    </div>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/parallax.min.js"></script>
    <script type="text/javascript" src="js/wow.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
</body>
</html>
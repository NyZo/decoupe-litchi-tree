<?php $namePage="pageStay"; $lang ="en"; ?>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Stay with us - The Litchi Tree</title>
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<![endif]-->

<?php include "css/css.php";?>
<?php include "css/animate.php";?>
</head>

<body>
    <div id="stay">
        <?php include "header.php";?>
            <div id="banner">
                <a href="index.php" title="The Litchi Tree" class="logo wow fadeIn"><img src="images/logo-menu.png" alt="The Litchi Tree"></a>
                <div class="caption">
                    <div class="wow fadeIn">
                        <div class="titrePage"><span>Have you already booked?</span>Stay with us</div>
                        <span class="fill">Please fill the form below</span>
                    </div>
                    <div class="form-wrapper">
                        <form id="formStay" class="wow fadeInUp" action="inc/send.php" method="POST">
                            <div>
                                <label for="name">Your name</label>
                                <input type="text" class="form-control" id="name" name="name" required>
                            </div>
                            <div>
                                <label for="email">Your e-mail</label>
                                <input type="email" class="form-control" id="email" name="email" required>
                            </div>
                            <div>
                                <label for="comments">Your comments</label>
                                <textarea class="form-control" id="comments" name="comments" required></textarea>
                            </div>
                            <div>
                                <button type="submit" class="btn-form">Send</button>
                                <input type="hidden" name="contact" value="1">
                                <input type="reset" class="reset" style="display: none;">
                            </div>
                        </form>
                        <div class="loading"></div>
                        <div class="msg"><p>Thank you, your request has been successful!</p></div>
                    </div>
                </div>
                <?php include "footer.php";?>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        jQuery(document).ready(function($){
            var $form = $("#formStay");
            $form.validate({
                onfocusout: false,
                focusInvalid: false,
                rules : {},
                highlight: function(element, errorClass, validClass){
                    var $el = $(element);
                    $el.parent("div").addClass("error");
                },
                unhighlight: function(element, errorClass, validClass) {
                    var $el = $(element);
                    $el.parent("div").removeClass("error");
                },
                submitHandler: function(form){
                    var post_url = $(form).attr("action");
                    var data = $(form).serialize();
                    var $loader = $(".loading");
                    var $wrapper = $(form).parent();
                    $.ajax({
                        type: "POST",
                        url: post_url,
                        data: data,
                        beforeSend: function(){
                            $wrapper.addClass('load');
                            $loader.fadeIn();
                        },
                        success : function(response){
                            $wrapper.find('.msg').fadeIn();
                            $loader.fadeOut();
                            setTimeout(function(){
                                $wrapper.find('.msg').hide();
                                $(form).find('.reset').trigger('click');
                                $wrapper.removeClass('load');
                            },3000);
                        },
                        error : function(error){
                            console.log(error);
                            $loader.fadeOut();
                        }
                    });
                    return false;
                }
            });
        });
    </script>
    
</body>
</html>
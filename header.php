<!-- HEADER START -->
<header id="header" <?php if ($namePage != "Home") echo("class='page'"); ?>>  
    <nav>
        <div class="right">
            <div class="openToogle wow fadeInRight"">MENU<span></span></div>
            <div class="menu">
                <div class="closeToogle"></div>
                <?php if ($lang == "fr") { ?>
                    <a href="accueil.php" title="The Litchi Tree" class="logo-menu"><img src="images/logo-menu.png" alt="The Litchi Tree"></a>
                <?php } else { ?>
                    <a href="index.php" title="The Litchi Tree" class="logo-menu"><img src="images/logo-menu.png" alt="The Litchi Tree"></a>
                <?php } ?>
                <ul>
                    <?php if ($lang == "fr") { ?>
                        <li><a href="page-decouvrir.php" title="Découvrir">Découvrir</a></li> 
                        <li><a href="page-fr-coktail-lounge.php" title="Cocktail & lounge">Cocktail & lounge</a></li> 
                        <li><a href="page-notre-table.php" title="Notre Table">Notre Table</a></li> 
                        <li><a href="page-reserver.php" title="Réserver">Réserver</a></li> 
                        <li><a href="page-explorer.php" title="Explorer">Explorer</a></li> 
                        <li><a href="page-itineraire.php" title="Itinéraire">Itinéraire</a></li>
                    <?php } else { ?>
                        <li><a href="page-take-the-journey.php" title="Take the journey">Take the journey</a></li> 
                        <li><a href="page-coktail-lounge.php" title="Cocktail & lounge">Cocktail & lounge</a></li> 
                        <li><a href="page-dine.php" title="Dine">Dine</a></li> 
                        <li><a href="page-stay.php" title="Stay with us">Stay with us</a></li> 
                        <li><a href="page-area.php" title="Area attractions">Area attractions</a></li> 
                        <li><a href="page-location.php" title="Location">Location</a></li>
                    <?php } ?>

                </ul>
                 <div class="contactRight">
                    <div>
                        <a href="tel:+261330342272" class="tel" title="00 261 33 03 422 72">00 261 33 03 422 72</a>
                        <a href="mailto:thelitchitree@hotmail.com" class="mail" title="thelitchitree@hotmail.com">thelitchitree@hotmail.com</a>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
        
    </nav>
</header>
<div class="content">
<?php if ($namePage !== "Home") { ?>
    <?php if ( !in_array($namePage , array("pageLocation", "pageItineraire", "pageStay", "pageReserver")) ) { ?>
        <div id="banner" 
        <?php if ($namePage == "pageJourney") { ?> class="bannerJourney"<?php } ?>
        <?php if ($namePage == "pageDecouvrir") { ?> class="bannerJourney"<?php } ?>
        <?php if ($namePage == "pageCocktail") { ?> class="bannerCocktail" <?php } ?>
        <?php if ($namePage == "pageCocktail-fr") { ?> class="bannerCocktail" <?php } ?>
        <?php if ($namePage == "pageDine") { ?> class="bannerDine" <?php } ?>
        <?php if ($namePage == "pageTable") { ?> class="bannerDine" <?php } ?>
        <?php if ($namePage == "pageArea") { ?> class="bannerArea"<?php } ?>
        <?php if ($namePage == "pageExplorer") { ?> class="bannerArea"<?php } ?>
        >


            <div class="wow fadeIn">
                <?php if ($lang == "fr") { ?>
                    <a href="accueil.php" title="The Litchi Tree" class="logo"><img src="images/logo-menu.png" alt="The Litchi Tree"></a>
                <?php } else { ?>
                    <a href="index.php" title="The Litchi Tree" class="logo"><img src="images/logo-menu.png" alt="The Litchi Tree"></a>
                <?php } ?>
            </div>
            <div class="caption wow fadeInUp">
                <div class="titrePage">
                    <?php if ($namePage == "pageJourney") { ?><span>The Litchi Tree boutique hotel</span>Take the journey<?php } ?>
                    <?php if ($namePage == "pageDecouvrir") { ?><span>The Litchi Tree boutique hotel</span>Découvrir<?php } ?>
                    <?php if ($namePage == "pageCocktail") { ?><span>After a day in the park</span>Cocktail & lounge<?php } ?>
                    <?php if ($namePage == "pageCocktail-fr") { ?><span>Après une journée dans le parc</span>Cocktail & lounge<?php } ?>
                    <?php if ($namePage == "pageDine") { ?><span>Original flavours mixed cuisine</span>Dine<?php } ?>
                    <?php if ($namePage == "pageTable") { ?><span>Saveurs originales cuisine métissée</span>Notre Table<?php } ?>
                    <?php if ($namePage == "pageArea") { ?><span>Explore the north of Madagascar</span>area attractions<?php } ?>
                    <?php if ($namePage == "pageExplorer") { ?><span>Explorez le nord de Madagascar</span>attractions de la région<?php } ?>
                    <?php if ($namePage == "pageLocation") { ?><?php } ?>
                </div>
            </div>
            <a href="contentPage" class="scroll scrollBot"></a>
        </div>

        <?php if ($namePage == "pageCocktail") { ?>
        <div class="cocktail-bg"></div>
        <div id="videoFond">
           <video autoplay loop id="video">
              <source src="video/video-cocktail.mp4" type="video/mp4"/>
              <source src="video/video-cocktail.webm" type="video/webm"/>
              <source src="video/video-cocktail.ogv" type="video/ogg"/>
            </video> 
        </div>
        <?php } ?>
        <?php if ($namePage == "pageCocktail-fr") { ?>
        <div class="cocktail-bg"></div>
        <div id="videoFond">
            <video autoplay loop id="video">
              <source src="video/video-cocktail.mp4" type="video/mp4"/>
              <source src="video/video-cocktail.webm" type="video/webm"/>
              <source src="video/video-cocktail.ogv" type="video/ogg"/>
            </video>
        </div>
        <?php } ?>

        <?php if ($namePage == "pageDine") { ?>
        <div class="dine-bg"></div>
        <div id="videoFond">
            <video autoplay loop id="video">
              <source src="video/video-dine.mp4" type="video/mp4"/>
              <source src="video/video-dine.webm" type="video/webm"/>
              <source src="video/video-dine.ogv" type="video/ogg"/>
            </video>
        </div>
        <?php } ?>
        <?php if ($namePage == "pageTable") { ?>
        <div class="dine-bg"></div>
        <div id="videoFond">
            <video autoplay loop id="video">
              <source src="video/video-dine.mp4" type="video/mp4"/>
              <source src="video/video-dine.webm" type="video/webm"/>
              <source src="video/video-dine.ogv" type="video/ogg"/>
            </video>
        </div>
        <?php } ?>

    <?php } ?>
<?php } ?>


<!-- HEADER END -->
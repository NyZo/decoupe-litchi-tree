$(document).ready(function() {	
	
	$(".openToogle").click(function() {
		$(".menu").addClass("active");
		$('body').addClass('active');
	});

	$(".closeToogle").click(function() {
		$(".menu").removeClass("active");
		$('body').removeClass('active');
	});

	// SCROLL //
	$(".scroll").click(function() {
		c = $(this).attr("href")
		$('html, body').animate({ scrollTop: $("#" + c).offset().top + 1}, 1000, "linear");
		return false
	})

	// scroll logo
	$(window).scroll(function() {
	  	var scroll = $(window).scrollTop();  
		if (scroll > 100) { 
		  $('body').addClass('top');
		}   
		else {
		  $('body').removeClass('top');
		}
	});

	// scroll logo
	$(window).scroll(function(){
		var posScroll = $(document).scrollTop();
		if(posScroll>250) 
			$('.blc-made').addClass('bof')
		else
			$('.blc-made').removeClass('bof')
	});

    $(".fixe .arrow").click(function() {
        $(".fixe").toggleClass('hide_child');
	});
	$(".height .arrow").click(function() {
        $(".height").toggleClass('hide_child');
	});
});

new WOW().init();

var lFollowX = 0,
    lFollowY = 0,
    x = 0,
    y = 0,
    friction = 1 / 30;

function moveBackground() {
  x += (lFollowX - x) * friction;
  y += (lFollowY - y) * friction;
  
  translate = 'translate(' + x + 'px, ' + y + 'px) scale(1.1)';

  $('.bg').css({
    '-webit-transform': translate,
    '-moz-transform': translate,
    'transform': translate
  });

  window.requestAnimationFrame(moveBackground);
}

$(window).on('mousemove click', function(e) {

  var lMouseX = Math.max(-100, Math.min(100, $(window).width() / 2 - e.clientX));
  var lMouseY = Math.max(-100, Math.min(100, $(window).height() / 2 - e.clientY));
  lFollowX = (10 * lMouseX) / 100; // 100 : 12 = lMouxeX : lFollow
  lFollowY = (10 * lMouseY) / 100;

});

moveBackground();
$(document).ready(function(){
    w=$(window).width()
    if (w>979){
        $('.parallax').each(function(index) {
            $(this).css({'z-index' : (index+10) });
        })
        $(window).scroll(function(e) {
            var scrolled = $(window).scrollTop()
            $('.parallax').each(function(index, element) {
                var vis = $(this);
                var initY = $(this).offset().top
                var visible = isInViewport(this)
                if(visible) {        
                    if (scrolled > initY ) {
                        vis.addClass('visible');
                    }else {
                        vis.removeClass('visible');     
                    }
                }else {
                    $(this).removeClass('visible');
                }
            })
        })
    };
});

function isInViewport(node) {
    var rect = node.getBoundingClientRect()
    return (
        (rect.height > 0 || rect.width > 0) &&
        rect.bottom >= 0 &&
        rect.right >= 0 &&
        rect.top <= (window.innerHeight || document.documentElement.clientHeight) &&
        rect.left <= (window.innerWidth || document.documentElement.clientWidth)
    )
};
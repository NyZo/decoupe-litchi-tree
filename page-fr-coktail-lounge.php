<?php $namePage="pageCocktail-fr"; $lang ="fr"; ?>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Cocktail & Lounge - The Litchi Tree</title>
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<![endif]-->

<?php include "css/css.php";?>
<?php include "css/animate.php";?>
</head>

<body>
    <div id="page">
        <?php include "header.php";?>
        <div id="contentPage">
        	<article class="parallax">
                <div class="inner_parallax">
    	        	<div class="wrapper wrapper2">
    	        		<div class="col2 wow fadeInLeft">
    	        		    <div class="slide-img">
                                <div class="photoPage">
                                    <img src="images/photo-4.jpg" alt="">
                                </div>
                                <div class="photoPage">
                                    <img src="images/photo-5.jpg" alt="">
                                </div>
                                <div class="photoPage">
                                    <img src="images/photo-6.jpg" alt="">
                                </div>
                                <div class="photoPage">
                                    <img src="images/photo-7.jpg" alt="">
                                </div>
                            </div>
    	        		</div>
    	        		<div class="col2 wow fadeInRight">
    	        		    <div class="text">
                                <div class="titre">
                                    <span>Nous offrons</span>UNE GRANDE SÉLECTION
                                </div>
                                <p>Après une journée dans le parc, venez apprécier un cocktail dans notre charmant petit salon rouge. Nous vous proposons une large sélection de vieux rhums malgaches, whiskies, gins et cocktails classiques.</p>
                            </div>
    	        		</div>
    	        		<div class="clear"></div>
    	        	</div>
                </div>
        	</article>
        </div>
        <?php include "footer.php";?>
    </div>



</body>
</html>
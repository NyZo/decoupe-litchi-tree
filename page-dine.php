<?php $namePage="pageDine"; $lang ="en"; ?>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Dine - The Litchi Tree</title>
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<![endif]-->

<?php include "css/css.php";?>
<?php include "css/animate.php";?>
</head>

<body>
    <div id="page">
        <?php include "header.php";?>
        <div id="contentPage">
        	<article class="parallax">
                <div class="inner_parallax">
    	        	<div class="wrapper wrapper2">
                        <div class="col2 wow fadeInLeft">
                            <div class="slide-img">
                                <div class="photoPage">
                                    <img src="images/photo-8.jpg" alt="">
                                </div>
                                <div class="photoPage">
                                    <img src="images/photo-9.jpg" alt="">
                                </div>
                                <div class="photoPage">
                                    <img src="images/photo-10.jpg" alt="">
                                </div>
                                <div class="photoPage">
                                    <img src="images/photo-11.jpg" alt="">
                                </div>
                                <div class="photoPage">
                                    <img src="images/photo-12.jpg" alt="">
                                </div>
                                <div class="photoPage">
                                    <img src="images/photo-13.jpg" alt="">
                                </div>
                            </div>
    	        	    </div>
    	        	    <div class="col2 wow fadeInLeft">
    	        	        <div class="titre">
    	        				<span>With Our</span>French Chef
    	        			</div>
    	        			<p>The french chef serves a original flavours mixed cuisine mingle with the discovery of our wide wine selection.<br/>We use exclusively local product.</p>
    	        	    </div>
    	        	    <div class="clear"></div>
    	        	</div>
                </div>
        	</article>
        </div>
        <?php include "footer.php";?>
    </div>
</body>
</html>
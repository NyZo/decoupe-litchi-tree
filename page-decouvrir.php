<?php $namePage="pageDecouvrir"; $lang ="fr"; ?>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Découvrir - The Litchi Tree</title>
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<![endif]-->

<?php include "css/css.php";?>
<?php include "css/animate.php";?>
</head>

<body>
    <div id="page">
        <?php include "header.php";?>
        <div id="contentPage">
        	<article class="intro parallax">
                <div class="inner_parallax">
    	        	<div class="wrapper">
    	        		<div class="col2 wow fadeInLeft" >
    		        		<div class="photoPage">
    		        			<img src="images/photo-1.jpg" alt="">
    		        		</div>
    	        		</div>
    	        		<div class="col2 wow fadeInRight">
    	        			<div class="titre">
    	        				<span>Notre histoire</span>Circa 1902
    	        			</div>
    	        			<p>The Litchi Tree, maison d’hôtes de charme, est situé sur les hauteurs de Joffreville à 800m d’altitude au pied du parc national de la montagne d’ambre. <br/>
                            A 45 minutes de route de l’aéroport de Diego Suarez, venez savourer un instant raffiné de calme et de sérénité dans cette ancienne bâtisse coloniale de 1902 qui fut autrefois la demeure du Maréchal Joffre et de l’amirauté française.
                            </p>
    	        		</div>
    	        		<div class="clear"></div>
    	        	</div>
                </div>
        	</article>
        	<article class="primary fixe parallax">
                <div class="inner_parallax">
                    <div class="slide-bg">
            	        <div class="banner1"></div>
            	        <div class="banner2"></div>
            	        <div class="banner3"></div>
                        <div class="banner4"></div>
            	    </div>
        			<div class="absolu">
                        <div class="wrapper wow fadeInUp display">
            				<div class="wrap">
            				    <div class="mask">
                                    <div class="titre">
                                        <span>Savourer un agréable</span> MOMENT DE CALME
                                    </div>
                                    <p>La propriété de 5 hectares comporte un grand nombre d’arbre à litchi et de manguier. Vous pouvez y voir des caméléons, geckos, boas et autres serpents. Laisser vous surprendre par la douce mélodie de la nature et contempler depuis notre terrasse la vue sur la baie de Diego Suarez et le canal du Mozambique.</p>
                                </div>
                                <div class="arrow"></div>
        					</div>
            			</div>
                    </div>
                </div>
        	</article>
        	<article class="secondary parallax">
                <div class="inner_parallax">
    	        	<div class="wrapper wrapper2">
    	        	    <div class="col2 wow fadeInLeft">
    	        	        <div class="titre">
    	        				<span>Témoin de</span>VOTRE BIEN ÊTRE
    	        			</div>
    	        			<p>Témoin de votre bien être, The Litchi Tree vous propose six chambres où les essences des bois nobles de Madagascar côtoient une décoration raffinée alliant confort et authenticité. Toutes nos chambres ont des salles de bain individuelles aux vastes douches équipées de chauffe-eau solaire afin de respecter au mieux notre environnement.</p>
    	        	    </div>
    	        	    <div class="col2 wow fadeInLeft">
                            <div class="slide-img">
                                <div class="photoPage">
                                    <img src="images/photo-2.jpg" alt="">
                                </div>
                                <div class="photoPage">
                                    <img src="images/photo-3.jpg" alt="">
                                </div>
                            </div>
    	        	    </div>
                        <div class="clear"></div>
                    </div>
                </div>
        	</article>
        </div>
        <?php include "footer.php";?>
    </div>
</body>
</html>
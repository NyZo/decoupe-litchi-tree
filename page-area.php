<?php $namePage="pageArea"; $lang ="en"; ?>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Area attractions - The Litchi Tree</title>
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<![endif]-->

<?php include "css/css.php";?>
<?php include "css/animate.php";?>
</head>

<body>
    <div id="page">
        <?php include "header.php";?>
        <div id="contentPage">
            <article class="primary fixe parallax">
                <div class="inner_parallax">
                    <div class="slide-bg slide-area">
                        <div class="banner1"></div>
                        <div class="banner2"></div>
                        <div class="banner3"></div>
                        <div class="banner4"></div>
                        <div class="banner5"></div>
                        <div class="banner6"></div>
                    </div>
                    <div class="absolu">
                        <div class="wrapper wow fadeInUp display">
                            <div class="wrap">
                                <div class="mask">
                                    <div class="titre">
                                        <span>National Park</span>The amber mountain
                                    </div>
                                    <p>Located 4km from the hotel, the Amber Mountain National Park is the first natural reserve of Madagascar. Streching at an altitude between 850 and 1475m, the park is well-known for providing a perfect environment to a lush endemic tropical forest with 1000 plant species, 5 crater lakes, its sacred waterfalls, its botanical garden and its seven lemur species. The Litchi Tree provides you with the 4x4 transfer.</p>
                                </div>
                                <div class="arrow"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        	<article class="primary height parallax">
                <div class="inner_parallax">
                    <div class="absolu">
            			<div class="wrapper wow fadeInUp">
            				<div class="wrap">
            				    <div class="mask">
                                    <div class="titre">
                                        <span>Ankarana special reserve &</span> The red tsingy
                                    </div>
                                    <p>The Ankarana massif, a 2500 hectares coral reef, is a continental shelf divided by numerous canyons. The underground watercourses have slowly dug hundreds of caves and tunnels where the Antakarana tribe used to take cover from conflicts. Stretching for 11kms, the Andrafiabe is the most famous of these caves. The Red Tsingy is a rocky formation due to erosion. Those hundreds of red rock needles offer a unique sight on Madagascar. The Litchi Tree is at your disposal to organise your excursion in Ankarana from the hotel.</p>
                                </div>
                                <div class="arrow"></div>
        					</div>
            			</div>
                    </div>
                </div>
        	</article>
        	<article class="secondary parallax">
                <div class="inner_parallax">
    	        	<div class="wrapper wrapper2">
    	        		<div class="col2 wow fadeInLeft">
    	        			<div class="titre">
    	        				<span>The 3 bays &</span>the emerald sea
    	        			</div>
    	        			<p>Bathed by the Indian Ocean, the wonderfull turquoise “Emeraude Sea” provides an ideal environment for an amazing underwater fauna. Discover the preserved beaches, and the French army remainings, by walking along the Sakalava Bay, the Dunes Bay, and the Pigeons Bay. The Litchi Tree can organise the discovering of the Emeraude Sea by boat or an excursion to the Three Bays for you. All departures are set from the hotel.</p>
    	        		</div>
    	        		<div class="col2 wow fadeInRight">
                            <div class="slide-img">
                                <div class="photoPage">
                                    <img src="images/photo-14.jpg" alt="">
                                </div>
                                <div class="photoPage">
                                    <img src="images/photo-15.jpg" alt="">
                                </div>
                                <div class="photoPage">
                                    <img src="images/photo-16.jpg" alt="">
                                </div>
                            </div>
    	        		</div>
    	        		<div class="clear"></div>
    	        	</div>
                </div>
        	</article>
        </div>
        <?php include "footer.php";?>
    </div>

</body>
</html>
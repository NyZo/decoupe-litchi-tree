<?php $namePage="Home"; $lang ="en"; ?>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Home - The Litchi Tree</title>
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<![endif]-->

<?php include "css/css.php";?>
<?php include "css/animate.php";?>
</head>

<body>
    <div id="home">
        <div class="bg"></div>
        <?php include "header.php";?>
        <div class="langue wow fadeInLeft">
            <ul>
                <li><a href="accueil.php">FR</a></li>
            </ul>
        </div>
        <div>
            <div class="wow fadeInDown" >
                <a href="index.php" title="The Litchi Tree"><img src="images/logo-home.png" alt="The Litchi Tree"></a>
                <div class="titrePage"><span>Madagascar</span> Amber mountain</div>
            </div>
            <a href="page-take-the-journey.php" title="Take the journey" class="btn wow fadeInUp" data-wow-delay="1s">
                <span>Take the journey</span>
            </a>
            <a href="page-stay.php" title="Stay with us" class="btn wow fadeInUp" data-wow-delay="1.5s">
                <span>Stay with us</span>
            </a>
            <a href="page-area.php" title="Area attractions" class="btn wow fadeInUp" data-wow-delay="2s">
                <span>Area attractions</span>
            </a>
        </div>
    </div>
    <div class="blc-made show">
        <a href="http://maki-agency.com/" title="Made by Maki Agency" target="_blank"> <span><strong>Made by</strong> Maki Agency</span></a>
    </div>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/parallax.min.js"></script>
    <script type="text/javascript" src="js/wow.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
</body>
</html>
<!-- FOOTER START -->

<footer id="footer">
    <div class="wrapper">
    	<div class="footer">
    		<div class="col">© <span>The Litchi Tree</span> 2017. All rights reserved</div>
    		<div class="col map"><i></i>Joffreville - Montagne d’Ambre - Madagascar</div>
    		<div class="col telf"><i></i><a href="tel:+261330342272" title="+261 330342272">+261 330342272</a></div>
    		<div class="col mailf"><i></i><a href="mailto:thelitchitree@hotmail.com" title="thelitchitree@hotmail.com">thelitchitree@hotmail.com</a></div>
            <div class="col made"><a href="http://maki-agency.com/" title="Made by Maki Agency" target="_blank">Made by Maki Agency</a></div>
    	</div>
    </div>
   
</footer>
</div>
<!-- FOOTER END -->
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/slick.min.js"></script>
<script type="text/javascript" src="js/wow.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script src="js/parallax.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        // Slider bg
        $('.slide-bg').slick({
            fade:true
        });

        // Slider image
        $('.slide-img').slick({
            dots: false,
            arrows: false
        });               
    });
</script>
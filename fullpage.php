
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	
	<link rel="stylesheet" type="text/css" href="css/jquery.fullPage.css" />
	<!--[if IE]>
		<script type="text/javascript">
			 var console = { log: function() {} };
		</script>
	<![endif]-->
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/fullpage.parallax.limited.min.js"></script>
	<script type="text/javascript" src="js/jquery.fullpage.extensions.min.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {

			// Initializing fullpage.js
			initialize(false);

			function initialize(hasScrollBar){
				$('#myContainer').fullpage({
					anchors: ['firstPage', 'secondPage', '3rdPage', '4thpage'],
					menu: '#menu',
					slidesNavigation: true,
					parallax: true,
					parallaxKey: 'YWx2YXJvdHJpZ28uY29tXzlNZGNHRnlZV3hzWVhnPTFyRQ==',
					parallaxOptions: {
						type: 'cover',
						percentage: 100,
						property: 'translate'
					},
					scrollingSpeed: 1000,
					autoScrolling: false,
					scrollBar: hasScrollBar,
					fitToSection:false
				});
			}

			$.fn.fullpage.parallax.init();
			$.fn.fullpage.parallax.setOption('offset', 100);
			$.fn.fullpage.parallax.setOption('type', 'cover');
			
		});
	</script>

	<style>
		
		#section1 h1{
			-webkit-text-shadow: none;
			text-shadow: none;
		}
		#slide1-1 h1{
			color: #444;
		}
		/*
		* Setting the backgrounds for each section / slide
		*/
		#section4 .fp-bg:before,
		#section2 .fp-bg:before{
			content: '';
			position: absolute;
			display: block;
			background: rgba(173, 173, 173, 0.2);
			top:0;
			bottom:0;
			height: 100%;
			width: 100%;
		}
		.fp-bg{
			background-size: cover;
			transform: translate3d(0,0,0);
			-webkit-backface-visibility: hidden; /* Chrome, Safari, Opera */
	    		backface-visibility: hidden;
		}
		#slide1-1 .fp-bg{
			background-image: url('img/parallax-1.jpg');
			background-position: center 80%;
		}
		#slide1-2 .fp-bg{
			background-image: url('img/parallax-2.jpg');
		}
		#slide1-3 .fp-bg{
			background-image: url('img/parallax-3.jpg');
		}
		#section2 .fp-bg{
			/* background-image: url('img/parallax-2.jpg');
			background-position: center 60%; */
			background-color: #f00;
		}
		#section4 .fp-bg{
			background-image: url('img/parallax-4.jpg');
		}				
	</style>
</head>
<body>

<ul id="menu" style="width: 1px; height: 1px; position: absolute; opacity: 0">
	<li data-menuanchor="firstPage"><a href="#firstPage">First slide</a></li>
	<li data-menuanchor="secondPage"><a href="#secondPage">Second slide</a></li>
	<li data-menuanchor="3rdPage"><a href="#3rdPage">Third slide</a></li>
	<li data-menuanchor="4thpage"><a href="#4thpage">Fourth slide</a></li>
</ul>

<ul class="preview-extensions-menu" style="width: 1px; height: 1px; position: absolute; opacity: 0">
	<li class="preview-more-extensions"><a href="http://alvarotrigo.com/fullPage/extensions/">More extensions</a></li><!--
	--><li class="preview-buy-extension"><a href="https://gum.co/fullpageParallax">Download extension</a></li>
</ul>

<div id="myContainer">

	<div class="section" id="section1">
		<div class="slide" id="slide1-1">
	        <div class="fp-bg"></div>
			<h1>PARALLAX</h1>
			<p>Use `parallax: true`. Made easy.</p>
            <div class="button" id="download">
            	<a href="https://gum.co/fullpageParallax" class="button-purchase">I want it!</a>
            </div>
            <p class="documentation">
            	<a href="https://github.com/alvarotrigo/fullPage.js/wiki/Extension---Parallax">Documentation</a>
            </p>

		</div>
		<div class="slide" id="slide1-2">
			<div class="fp-bg"></div>
			<div class="intro">
				<h1>fullPage.js</h1>
				<img src="imgs/parallax/trusted.png" />
			</div>
		</div>
		<div class="slide" id="slide1-3">
			<div class="fp-bg"></div>
			<div class="intro">
				<h1>Free support</h1>
				<p>Extension issues are supported at no extra cost!</p>
			</div>
		</div>
	</div>
	<div class="section" id="section2">
		<div class="fp-bg" id="section2"></div>
		<div class="intro">
			<h1>FLEXIBLE</h1>
			<p>Put not limits to yourself</p>
			<p>Use parallax even without scroll bar!</p>
		</div>
	</div>
	<div class="section" id="section3">
		<div class="slide" id="slide3-1">
			<div class="fp-bg"></div>
			<div class="intro">
				<h1>TAKE CONTROL</h1>
				<p>Totally configurable.</p>
				<p>For sections & slides! </p>
			</div>
		</div>
		<div class="slide" id="slide3-2">
			<div class="fp-bg"></div>
			<div class="intro">
				<h1>GUARRANTEE</h1>
				<p>30 days money back guarratee if no domain was activated.</p>
			</div>
		</div>
		<div class="slide" id="slide3-3">
			<div class="intro">
				<div class="fp-bg"></div>
				<h1>TRUSTED</h1>
				<p>Join thoudands of other developers who trusted fullPage.js extensions!</p>
			</div>
		</div>
		<div class="slide" id="slide3-4">
			<div class="fp-bg"></div>
			<div class="intro">
				<h1>DOCUMENTED</h1>
				<p>If fullPage.js is known for one thing it's for its great documentation!</p>
			</div>
		</div>
	</div>
	<div class="section" id="section4">
		<div class="fp-bg"></div>
		<div class="intro">
			<span class="col">© <span>The Litchi Tree</span> 2017. All rights reserved</span>
			<span class="col map"><i></i>Joffreville - Montagne d’Ambre - Madagascar</span>
    		<!-- <span class="col telf"><i></i><a href="tel:+261330342272" title="00 261 33 03 422 72">00 261 33 03 422 72</a></span> -->
    		<!-- <div class="col mailf"><i></i><a href="mailto:thelitchitree@hotmail.com" title="thelitchitree@hotmail.com">thelitchitree@hotmail.com</a></div> -->
            <!-- <div class="col made"><a href="http://maki-agency.com/" title="Made by Maki Agency" target="_blank">Made by Maki Agency</div> -->
		</div>
	</div>
	
</div>
<!-- don't touch it -->
<input type="hidden" id="_4" value="3" />

</body>
</html>
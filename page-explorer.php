<?php $namePage="pageExplorer"; $lang ="fr"; ?>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Explorer - The Litchi Tree</title>
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<![endif]-->

<?php include "css/css.php";?>
<?php include "css/animate.php";?>
</head>

<body>
    <div id="page">
        <?php include "header.php";?>
        <div id="contentPage">
            <article class="primary fixe parallax">
                <div class="inner_parallax">
                    <div class="slide-bg slide-area">
                        <div class="banner1"></div>
                        <div class="banner2"></div>
                        <div class="banner3"></div>
                        <div class="banner4"></div>
                        <div class="banner5"></div>
                        <div class="banner6"></div>
                    </div>
                    <div class="absolu">
                        <div class="wrapper wow fadeInUp display">
                            <div class="wrap">
                                <div class="mask">
                                    <div class="titre">
                                        <span>Le parc national</span>de la Montagne d’Ambre
                                    </div>
                                    <p>Situé 4km de l’hôtel, le parc national de la Montagne d’Ambre est le premier parc créé à Madagascar protégeant une forêt tropicale. Culminant à une altitude comprise entre 850 et 1475m, le parc est connu pour son abondante flore tropicale endémique, et compte par moins de 1000 espèces de plantes, 5 lacs de cratères, plusieurs cascades sacrées, 7 espèces de lémuriens ainsi que d’un jardin botanique. The Litchi Tree vous assure un transfert en 4/4.</p>
                                </div>
                                <div class="arrow"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        	<article class="primary height parallax">
                <div class="inner_parallax">
                    <div class="absolu">
            			<div class="wrapper wow fadeInUp">
            				<div class="wrap">
            				    <div class="mask">
                                    <div class="titre">
                                        <span>Les Tsingy rouges et</span> La réserve de l’Ankarana
                                    </div>
                                    <p>Récif corallien de 2500 hectares, le massif de l’Ankarana est un plateau découpé par de nombreuses gorges. Parcouru par plusieurs cours d’eau souterrains, le massif est parsemé de plus d’une centaine de grottes et de galeries, où se réfugièrent jadis lors de conflits la population des Antakaranas. La plus fameuse de ces grottes est celle d’Andrafiabe, longue de 11km. Les « tsingy » sont des formations karstiques offrant la superbe vision de centaines d’aiguilles et de pics calcaires dressés vers le ciel. The Litchi Tree peut vous organiser vos journées d’expédition dans l’Ankarana au départ de l’hôtel.</p>
                                </div>
                                <div class="arrow"></div>
        					</div>
            			</div>
                    </div>
                </div>
        	</article>
        	<article class="secondary parallax">
                <div class="inner_parallax">
    	        	<div class="wrapper wrapper2">
    	        		<div class="col2 wow fadeInLeft">
    	        			<div class="titre">
    	        				<span>La mer d’émeraude et</span>les trois baies
    	        			</div>
    	        			<p>Baignée par les eaux de l’océan indien, cette mer d’un magnifique bleu turquoise transparent vous ouvre ses magnifiques fonds sous marins. Découvrez les plages sauvages et les vestiges de l’armée française en vous promenant sur la baie des Sakalava, la baie des dunes et la baie des pigeons. The Litchi Tree peut vous organiser la découverte de la mer d’émeraude en bateau ou la visite par la route des trois baies au départ de l’hôtel.</p>
    	        		</div>
    	        		<div class="col2 wow fadeInRight">
                            <div class="slide-img">
                                <div class="photoPage">
                                    <img src="images/photo-14.jpg" alt="">
                                </div>
                                <div class="photoPage">
                                    <img src="images/photo-15.jpg" alt="">
                                </div>
                                <div class="photoPage">
                                    <img src="images/photo-16.jpg" alt="">
                                </div>
                            </div>
    	        		</div>
    	        		<div class="clear"></div>
    	        	</div>
                </div>
        	</article>
        </div>
        <?php include "footer.php";?>
    </div>

</body>
</html>
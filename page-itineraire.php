<?php $namePage="pageItineraire"; $lang ="fr"; ?>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Itineraire - The Litchi Tree</title>
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<![endif]-->

<?php include "css/css.php";?>
<?php include "css/animate.php";?>
</head>

<body>
    <div class="location">
        <?php include "header.php";?>
        <div id="banner">
            <a href="index.php" title="The Litchi Tree" class="logo wow fadeIn"><img src="images/logo-menu.png" alt="The Litchi Tree"></a>
            <div class="primary caption">
               <div class="wrapper wow fadeInUp display">
                    <div class="wrap">
                        <div class="mask">
                            <h2>THE LITCHI TREE</h2>
                            <p>Joffreville-Montagne d'Ambre - Madagascar<br/>
                            Tél. <a href="tel:+261330342272" title="+261 330342272">+261 330342272</a><br/>
                            Email : <a href="mailto:thelitchitree@hotmail.com" title="thelitchitree@hotmail.com">thelitchitree@hotmail.com</a><br/><br/></p>
                            <p>The Litchi Tree est l'établissement le plus proche de l'entrée du parc national de la montagne d'ambre (4km), situé à 800m d'altitude Joffreville est un oasis de fraîcheur.</p>
                            <h3>Par la route :</h3>
                            <p>1h de Diego Suarez <span>//</span> 50 mn de l'aéroport <span>//</span> 2h des tsingys rouges, 3h30 de l'Ankarana <span>//</span> Environ 5h de Nosy Be</p>
                            <h3>Par le ciel :</h3>
                            <p>Vol quotidien pour la capitale <span>//</span> Vol hebdomadaire pour Mayotte et Nosy Be</p>
                        </div>
                    </div>
                </div>
            </div>
            <?php include "footer.php";?>
        </div>
    </div>
</body>
</html>
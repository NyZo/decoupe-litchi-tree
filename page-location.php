<?php $namePage="pageLocation"; $lang ="en"; ?>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Location - The Litchi Tree</title>
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<![endif]-->

<?php include "css/css.php";?>
<?php include "css/animate.php";?>
</head>

<body>
    <div class="location">
        <?php include "header.php";?>
        <div id="banner">
            <a href="index.php" title="The Litchi Tree" class="logo wow fadeIn"><img src="images/logo-menu.png" alt="The Litchi Tree"></a>
            <div class="primary caption">
               <div class="wrapper wow fadeInUp display">
                    <div class="wrap">
                        <div class="mask">
                            <h2>THE LITCHI TREE</h2>
                            <p>Joffreville-Montagne d'Ambre - Madagascar<br/>
                            Tél. <a href="tel:+261330342272" title="+261 330342272">+261 330342272</a><br/>
                            Email : <a href="mailto:thelitchitree@hotmail.com" title="thelitchitree@hotmail.com">thelitchitree@hotmail.com</a><br/><br/></p>
                            <p>The Litchi Tree is the closest accomodation from the entrance of the amber mountain national park (4km). At 800 m height above sea level, the climate is really pleasant.</p>
                            <h3>By car :</h3>
                            <p>1h to Diego Suarez <span>//</span> 50 mn to the airport <span>//</span> 2h to the red tsingy, 3h30 to Ankarana <span>//</span> About 5h from Nosy Be</p>
                            <h3>By plane :</h3>
                            <p>Daily flight to the capital <span>//</span> Weekly flight to Mayotte and Nosy Be</p>
                        </div>
                    </div>
                </div>
            </div>
            <?php include "footer.php";?>
        </div>
    </div>


</body>
</html>
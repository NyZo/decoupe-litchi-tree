<style>
@font-face {
  font-family: 'akr';
  src: url('fonts/akrobat-regular.eot?#iefix') format('embedded-opentype'),  
  		url('fonts/akrobat-regular.otf')  format('opentype'),
	    url('fonts/akrobat-regular.woff') format('woff'), 
	    url('fonts/akrobat-regular.ttf')  format('truetype'), 
	    url('fonts/akrobat-regular.svg#Akrobat-Regular') format('svg');
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: 'dinpl';
  src: url('fonts/dinpro-light.eot?#iefix') format('embedded-opentype'),  
  url('fonts/dinpro-light.otf')  format('opentype'),
	     url('fonts/dinpro-light.woff') format('woff'), 
	     url('fonts/dinpro-light.ttf')  format('truetype'), 
	     url('fonts/dinpro-light.svg#DINPro-Light') format('svg');
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: 'dinpr';
  src: url('fonts/dinpro-regular.eot?#iefix') format('embedded-opentype'),  
  url('fonts/dinpro-regular.otf')  format('opentype'),
	     url('fonts/dinpro-regular.woff') format('woff'), 
	     url('fonts/dinpro-regular.ttf')  format('truetype'), 
	     url('fonts/dinpro-regular.svg#DINPro-Regular') format('svg');
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: 'popl';
  src: url('fonts/poppins-light.eot?#iefix') format('embedded-opentype'),  
  url('fonts/poppins-light.woff') format('woff'), 
  url('fonts/poppins-light.ttf')  format('truetype'), 
  url('fonts/poppins-light.svg#Poppins-Light') format('svg');
  font-weight: normal;
  font-style: normal;
}

body { background: #151515; font: 16px/30px dinpl; letter-spacing:1px;color: #fff; padding: 0; margin: 0; position: relative; overflow-x: hidden; }
img {  display: block; margin: auto; max-width:100%}
p { margin: 0 0 20px }
ul { margin: 0; padding: 0 }
li { list-style-position: inside; }
input, textarea, button { outline: none; border: none; letter-spacing: normal; font-family: 'dinpl'; font-size: 15px }
input:focus, textarea:focus { border: none; }
a { text-decoration: none; color: #d5b045 }
a:hover { color: inherit; }
button { cursor: pointer; color: #fff; background: #d5b045; font-size: 16px; height: 55px; padding: 15px 28px; }
button:hover { background: #b38b19 }

body a,body:after, body:before, span:before, span:after, a:after, a:before,.content:after, .btn, button, form label.error,.form-wrapper form  { -webkit-transition:all 400ms ease-in-out; -moz-transition:all 400ms ease-in-out; -ms-transition:all 400ms ease-in-out; transition:all 400ms ease-in-out; }

/*<?php if ($namePage !== "Home") { ?>
body:before { content: ''; position: fixed; top: 0; left: 0; background: rgba(0,0,0,0.8); width: 100%; height: 185px; opacity: 0; z-index: 1; }
body.top:before { opacity: 1 }
<?php } ?>*/

.wrapper { max-width: 1145px; margin: auto; padding: 0 20px;}
.row { margin-left: -30px; margin-right: -30px;}
.col2 { width: 45%; float: left; padding: 0 2.5%;}

.btn { background: transparent; border: 1px solid #fff; font:16px/24px dinpr; color: #fff; text-transform: uppercase; display: inline-block;padding: 14px 20px;}
.btn:hover { background: #d5b045; border-color: #d5b045;}
.clear { clear: both;}
body:after { content:''; display: block; background: rgba(0,0,0,0.6); width: 100%; height: 100%; position: fixed; top: 0; left: 0; opacity: 0; visibility: hidden; z-index: 6;}
body.active:after {  opacity: 1; visibility: visible;}

.titrePage { font: 48px/65px dinpl; color: #d5b045; letter-spacing: 2.5px; text-transform: uppercase; margin-top: 35px; font-weight: normal;}
.titrePage span { font: 18px/24px popl; display: block; letter-spacing: 1.5px; text-transform: none; color: #fff }
.titrePage span:before { content: ''; display: inline-block; background: #fff; width: 10%; height: 1px; margin-right: 20px; top: -5px; position: relative;  }
.titrePage span:after { content: ''; display: inline-block; background: #fff; width: 10%; height: 1px; margin-left: 20px; top: -5px; position: relative;  }

.titre { font: 36px/45px dinpl; color: #d5b045; letter-spacing: 2.5px; text-transform: uppercase;  margin-bottom: 50px; font-weight: normal; position: relative;}
.titre span { font: 16px/20px popl; display: block; letter-spacing: 1.5px; text-transform: none; color: #ababab }
.titre:after { content: ''; display: block; background: #fff; width: 60px; height: 1px;    margin-top: 22px; }

/*** Homepage ***/
#home { width:100%; height: 100vh; text-align: center; overflow: hidden; position: relative; }
#home .bg { width: 100%; height: 100vh; position: absolute; top: 0; left: 0;  z-index: -1;  background: url(images/bg-home.jpg) no-repeat center; background-size: cover;  transform: scale(1.1); }
#home .content { display: table; width: 100%; height: 100% }
#home .content>div { display: table-cell; vertical-align: middle; }

#home .titrePage { font-size: 60px; line-height: 64px;  margin-bottom: 107px; }
#home .titrePage span { font-size: 30px; line-height: 34px }
#home .titrePage span:before { top: -11px;  margin-right: 8px; }
#home .titrePage span:after { top: -11px; margin-left: 8px; }
#home .btn { min-width: 198px; margin: 0 23px; position: relative; -webkit-transition:all 800ms ease-in-out; -moz-transition:all 800ms ease-in-out; -ms-transition:all 800ms ease-in-out; transition:all 800ms ease-in-out; }
#home .btn span { position: relative; z-index: 1 }
#home .btn:before { content: ""; background: #d5b045; width: 100%; height: 0; position: absolute; top: 0; left: 0 }
#home .btn:hover { background: transparent; border-color: #fff}
#home .btn:hover:before { height: 100%  }

.langue { position: fixed; top: 60px; left: 60px; z-index: 10;}
.langue li { list-style: none; line-height: 16px; }
.langue li a { color: #fff;line-height: 16px; }

/*** Sidebar ***/
#header { position: relative; z-index: 10 }
.openToogle { height: 15px; width: 80px; position: fixed; right: 60px; top: 60px; cursor: pointer; z-index: 2; line-height: 18px; font-family: popl; text-align: left; }
.openToogle span { width: 24px; height: 1px; position: absolute; display: block; background: #fff; top: 0; right: 0  }
.openToogle span:before { content: ''; display: block; background: #fff; height: 1px; width: 24px; position: absolute; top: 7px }
.openToogle span:after { content: ''; display: block; background: #fff; height: 1px; width: 24px; position: absolute; top: 14px }
.closeToogle { width: 20px; height: 20px;position: absolute; right: 60px; top: 38px; cursor: pointer;  }
.closeToogle:before { content: ''; display: block; background: #fff; height: 1px; width: 20px; position: absolute; top: 9px; 
transform: rotate(45deg); 
/* W3C */
-webkit-transform: rotate(45deg); /* Safari & Chrome */
-moz-transform: rotate(45deg); /* Firefox */
-ms-transform: rotate(45deg); /* Internet Explorer */
-o-transform: rotate(45deg); /* Opera */);}
.closeToogle:after { content: ''; display: block; background: #fff; height: 1px; width: 20px; position: absolute; top: 9px; 
transform: rotate(-45deg); 
/* W3C */
-webkit-transform: rotate(-45deg); /* Safari & Chrome */
-moz-transform: rotate(-45deg); /* Firefox */
-ms-transform: rotate(-45deg); /* Internet Explorer */
-o-transform: rotate(-45deg); /* Opera */);}

.logo-menu { margin: 40px 0 49px; display: inline-block; }
.logo-menu:hover { opacity: .7 }
.menu { background: #151515; width: 350px; height: 100%; position: fixed; top: 0; right:-350px; z-index: 6; text-align: center; overflow: auto; -webkit-transition:all 1s ease-in-out; -moz-transition:all 1s ease-in-out; -ms-transition:all 1ms ease-in-out; transition:all 1s ease-in-out;}
.menu.active { right: 0 }
.menu ul { padding: 0 40px; }
.menu li { list-style: none; border-bottom: 1px dotted #353535 }
.menu li a { font: 16px/24px akr; color: #fff; text-transform: uppercase; display: block; padding: 18px 0 }
.menu li:last-child { border-bottom: none; }
.menu li a:hover { color: #d5b045 }

.contactRight {  position: absolute; width: 100%; bottom: 0; background: #151515;letter-spacing: normal; }
.contactRight>div { border-top: 1px solid #353535; padding: 28px 0 20px; margin: 0 40px}
.contactRight a { color: #a0a0a0; font-size: 12px; }
.contactRight a:hover { color: #d5b045 }
.contactRight a:before { content: ''; display: block; width: 100%; height: 10px; margin-bottom: 4px;  }
.tel { float: left; }
.tel:before { background: url(images/ico-tel.svg) no-repeat center; }
.mail { float: right; }
.mail:before { background: url(images/ico-mail.svg) no-repeat center; }

/*** Page Interne ***/
#banner { text-align: center; height: 100vh; background-size: cover; display: table; width: 100%; position: fixed; top: 0; z-index: 2;}
.logo { display: block; width: 150px; position: absolute; top:40px; left: 0; right: 0; margin: auto; z-index: 2; }
.logo:hover { opacity: .7 }
.caption { display: table-cell; vertical-align: middle; width: 100%; padding: 0 20px }
.scrollBot { position: absolute; bottom: 80px;  margin: auto; left: 0; right: 0; display:block; background:url(images/mouse.svg); width: 15px; height: 23px; animation: MoveUpDown 1s infinite ease-in-out;}
.scrollBot:hover { animation-play-state: paused; }
@keyframes MoveUpDown {
  0% {
    bottom: 80px;
  }
  50% {
    bottom: 90px;
  }
  100% {
    bottom: 80px;
  }
}

#contentPage { background: #151515; position: relative; margin-top: 100vh; z-index: 3;}
/*article { display: flex; width: 100%; height: 100vh; padding:0;}*/
.parallax { position: relative; height: 100vh;}
.inner_parallax { display: flex; width: 100%; height: 100%;}
.visible .inner_parallax { position: fixed; top: 0; left: 0; right: 0; height: 100vh;}

.intro { background: #000;}
.primary { text-align: center; background-size: cover; padding: 0; position: relative;-webkit-transition:all 600ms ease-in-out; -moz-transition:all 600ms ease-in-out; -ms-transition:all 600ms ease-in-out; transition:all 600ms ease-in-out;}
.primary .wrapper { max-width:617px; background: rgba(0,0,0,0.4); padding: 20px; position: relative; z-index: 5;-webkit-transition:all 600ms ease-in-out; -moz-transition:all 600ms ease-in-out; -ms-transition:all 600ms ease-in-out; transition:all 600ms ease-in-out;}
.primary .wrap { background: #fff; padding: 57px 50px; -webkit-transition:all 600ms ease-in-out; -moz-transition:all 600ms ease-in-out; -ms-transition:all 600ms ease-in-out; transition:all 600ms ease-in-out;}
.primary p { margin:0; color: #151515;}
.primary .titre span { color:#151515;}
.primary .titre:after { background: #151515; margin: 22px auto 0;}
.fixe { min-height: 535px;}
.height { min-height: 650px; background: url(images/bg-primary-a.jpg) 50% no-repeat; -webkit-background-size:cover !important; -moz-background-size:cover !important; background-size:cover !important; background-attachment: fixed;}
    
/* Slide BG */
.slide-bg, .slide-area { width:100%; height: 100vh;}
.slide-bg .slick-list { width:100%; height:100%;}
.slide-bg .slick-track { width:100%; height:100%;}
.slide-bg .banner1 { background:url(images/bg-primary1.jpg) 50% 50% no-repeat; -webkit-background-size:cover !important; -moz-background-size:cover !important; background-size:cover !important;}
.slide-bg .banner2 { background:url(images/bg-primary2.jpg) 50% 50% no-repeat; -webkit-background-size:cover !important; -moz-background-size:cover !important; background-size:cover !important;}
.slide-bg .banner3 { background:url(images/bg-primary3.jpg) 50% 50% no-repeat; -webkit-background-size:cover !important; -moz-background-size:cover !important; background-size:cover !important;}
.slide-bg .banner4 { background:url(images/bg-primary4.jpg) 50% 50% no-repeat; -webkit-background-size:cover !important; -moz-background-size:cover !important; background-size:cover !important;}
.mask { -webkit-transition:all 600ms ease-in-out; -moz-transition:all 600ms ease-in-out; -ms-transition:all 600ms ease-in-out; transition:all 600ms ease-in-out;}
.mask span { font: 16px/24px popl; color: #d5b045 }
    
/* Slide Arrea */
.slide-area .banner1 { background:url(images/slider-area1.jpg) 50% 50% no-repeat; -webkit-background-size:cover !important; -moz-background-size:cover !important; background-size:cover !important;}
.slide-area .banner2 { background:url(images/slider-area2.jpg) 50% 50% no-repeat; -webkit-background-size:cover !important; -moz-background-size:cover !important; background-size:cover !important;}
.slide-area .banner3 { background:url(images/slider-area3.jpg) 50% 50% no-repeat; -webkit-background-size:cover !important; -moz-background-size:cover !important; background-size:cover !important;}
.slide-area .banner4 { background:url(images/slider-area4.jpg) 50% 50% no-repeat; -webkit-background-size:cover !important; -moz-background-size:cover !important; background-size:cover !important;}
.slide-area .banner5 { background:url(images/slider-area5.jpg) 50% 10% no-repeat; -webkit-background-size:cover !important; -moz-background-size:cover !important; background-size:cover !important;}
.slide-area .banner6 { background:url(images/slider-area6.jpg) 50% 50% no-repeat; -webkit-background-size:cover !important; -moz-background-size:cover !important; background-size:cover !important;}

.hide_child .arrow { transform: rotate(180deg); top: -240px; left: 0; right: 0; bottom: inherit}
.hide_child .mask { opacity: 0; }
.hide_child .wrap { background: none; padding: 0;}
.hide_child .wrapper { background: none; padding: 0;}

/* ARROW */
.arrow { background: url(images/arrow.png) 50% no-repeat; width: 35px; height: 21px; margin: 0 auto; position: absolute; bottom: 40px; left: 0; right: 0; cursor: pointer; -webkit-transition:all 50ms ease-in-out; -moz-transition:all 50ms ease-in-out; -ms-transition:all 50ms ease-in-out; transition:all 50ms ease-in-out;}   
.absolu { display: flex; width: 100%; height: 100vh; position: absolute; left: 0; right: 0; top: 0; z-index: 5;}
.secondary { background: #151515; text-align: right;}
.secondary .titre { margin-bottom: 72px;  }
.secondary .titre:after { position: absolute; right: 0; top: 64px; }

#stay { background: url(images/banner-stay.jpg) no-repeat; width: 100%; height: 100vh; -webkit-background-size:cover !important; -moz-background-size:cover !important; background-size:cover !important;}
.bannerJourney { background:  url(images/banner-journey.jpg) 50% 10% no-repeat; background-size: cover; height: 100vh;}
.bannerArea { background:  url(images/banner-area.jpg) 50% 10% no-repeat; background-size: cover; height: 100vh;}
.fill { background: #d5b045; display: inline-block; letter-spacing: normal; padding: 4px 20px;    margin-top: 64px; }
.caption form { display: table; width: 100%; letter-spacing: normal; margin: 68px auto 0; max-width: 1152px}
.caption form>div { display: inline-block; text-align: left; margin: 0 2.1%; vertical-align: bottom;  }
.caption form>div:nth-child(1) { width: 20.8% }
.caption form>div:nth-child(2) { width: 20.8% }
.caption form>div:nth-child(3) { width: 33% }
.caption form>div:nth-child(4) { width: 7.5% }

form label                      { display: block; }
form .form-control              { height: 55px; padding: 0 20px; box-sizing: border-box; width: 100%; display: block; line-height: 55px;}
form label.error                { position: absolute; left: 0; margin-left: 0; font-size: .8em; opacity: 1; line-height: normal; height: 100%; width: 100%; top: 0; padding: 17.5px 20px 0; margin-top: 30px; background-color: #d9534f;}
form div.error                  { position: relative; overflow: hidden; }
form .error input[type="text"],
form .error input[type="email"],
form .error textarea            { background-color: #d9534f; color: #fff }
form .error input[type="text"]:focus ~ .error,
form .error input[type="email"]:focus ~ .error,
form .error textarea:focus ~ .error         { opacity: 0; }
.form-wrapper   { position: relative; }
.form-wrapper form        { opacity: 1 }
.form-wrapper.load form   { opacity: 0 }
.form-wrapper .msg        { top: -5em; position: relative; display: none} 
.loading                  { position: absolute; background: url(images/loading.gif) no-repeat 50% 50%; width: 100%; left: 0; top: 0; height: 100%; background-size: 60px; display: none }

video { width: 100%; height: 100% ; top: 0; left: 0; object-fit: cover; position: relative;}
#videoFond { height: 100vh; overflow: hidden; position: fixed; width: 100%; top: 0; left: 0;}
#videoFond:before { content: ''; background: rgba(0,0,0,0.5); position: absolute; width: 100%; height: 100%; z-index: 1 }
.cocktail-bg { background: url(images/banner-cocktail.jpg) 28% no-repeat; -webkit-background-size:cover !important; -moz-background-size:cover !important; background-size:cover !important; height: 100vh; overflow: hidden; position: fixed; width: 100%; top: 0; left: 0;}
.dine-bg { background: url(images/banner-dine.jpg) 28% no-repeat; -webkit-background-size:cover !important; -moz-background-size:cover !important; background-size:cover !important; height: 100vh; overflow: hidden; position: fixed; width: 100%; top: 0; left: 0;}

/* Location */
.location { background: url(images/banner-location.jpg) no-repeat !important; background-size: cover !important; margin-top: -2px !important;}
.location #banner { position: relative !important;}
.location .wrapper { max-width: 850px;} 
.location .primary { padding: 170px 0;}
.location h2 { font: 30px/35px dinpl; color: #d5b045; letter-spacing: 2.5px; text-transform: uppercase; margin-top: 0;}
.location h3 { font: 20px/25px dinpl; color: #d5b045; letter-spacing: 2.5px; text-transform: uppercase; margin-bottom: 5px;}
.location ul { margin-bottom: 20px;}
.location li { margin: 0; color: #151515;}

/*** Footer ***/
#footer { background: #151515; position: absolute; left: 0; right: 0; bottom: 0; z-index: 6;}    
#footer .wrapper { max-width: 1262px; width: 95%;}
.footer { color: #a0a0a0; font-size: 14px; display: table;  width: 100%; border-top: 1px solid #a0a0a0; padding: 16px 0 20px; letter-spacing: normal;}
.footer .col { display: table-cell; padding-right: 40px; }
.footer .col:nth-child(5) { text-align: right; padding-right: 0; }
.footer a { color: #a0a0a0 }
.footer a:hover, .footer span { color: #d5b045 }
.footer a.maki { color: #d5b045 }
.footer a.maki:hover { color: #a0a0a0 }
i { display: inline-block; margin-right: 16px; position: relative; }
.map i { width: 9px; height: 13px; background: url(images/ico-map.svg); top: 2px; }
.telf i { width: 10px; height: 10px; background: url(images/ico-tel.svg); }
.mailf i { width: 11px; height: 8px; background: url(images/ico-mail.svg); }
.made a:before { content: ''; background: url(images/logo-maki.png) 50% no-repeat; width: 13px; height: 13px; display: inline-block;; vertical-align: middle; margin:0 15px 2px -2px;}

#stay #footer, .location #footer { position: absolute; width: 100%; bottom: 0; left: 0; background: #151515 }
#stay .footer, .location .footer { border-color: #fff; border: none; }




    
/*** Responsive ***/
@media (max-width: 1300px) {
    .footer .col { padding-right: 0px; }
}
@media (max-width: 1200px) {
    .caption form>div { margin: 0 1.2%; }
    .footer .col { display: block; text-align: left }
    .footer .col:nth-child(1) { text-align: center; margin-bottom: 15px; }
    .footer .col:nth-child(2), .footer .col:nth-child(3),.footer .col:nth-child(4),.footer .col:nth-child(5) { width: 38%; float: left; padding-left: 12%;}
    .footer .col:nth-child(5) { text-align: left; }
    .hide_child .arrow { top: -150px; bottom: inherit;}
    .secondary { background: #151515;}
    .caption form { text-align: left; }
    .wrapper2 { width: 92%; padding: 0;}
    .location #footer .wrapper { width: 100% !important;}
}
@media (max-width: 1024px) {
    #videoFond { display: none; }
    #stay .caption { display: block; width: auto; padding: 15em 20px 5em;}
    #stay #footer { position: static;}
    .contactRight { display: none; }
    .secondary .titre { font-size: 35px;}
	.cocktail-bg 	{ background-image:url(images/banner-cocktail-mobile.jpg); background-position:center center}
	.dine-bg 		{ background-image:url(images/banner-dine-mobile.jpg); background-position:center center }
	.photoPage img { display:block; width:100%; margin-left:-1px}
	
	
}

@media (max-width: 1023px) { 
	

 }
@media (max-width: 979px) { 
    .openToogle { position: absolute;}
    #banner { position: relative;}
    #contentPage { margin-top: 0;}    
    .fill { margin-top: 35px; }
    .caption form {  margin-top: 35px}
    #home .btn { min-width: 140px; }
    article, .primary, article:last-child { padding: 50px 0; height: inherit;}
    .parallax { height: inherit;}
    .arrow { bottom: 30px;}
    .height .absolu { position: relative !important;}
    
    .titre:after { margin: 22px auto 0 }
    .secondary .titre:after { position: static; }
    .secondary .titre { margin-bottom: 50px;}

    .col2 { width: 100%; float: none; padding: 0; text-align: center; }
    .col2:first-child { margin-bottom: 60px;}
    .fixe, .height { padding: 0 !important; }

    .telf i, .mailf i { display: none; }
    .telf a { margin-top: 10px; min-width: 200px; }
    .telf a, .mailf a { border: 1px solid #a0a0a0; padding: 10px 20px 12px; display: inline-block; margin-bottom: 15px; min-width: 200px;}
    
    .location .primary { padding: 70px 0 120px;}
    .location .wrapper { max-width: 100%; padding: 0; margin: 0 !important;}
    .primary .wrap { padding: 57px 40px;}

    #footer { position: relative;}
    #footer .wrapper { width: 93%;}
    .footer .col { text-align: center; }
    .footer .col:nth-child(1) {   margin-bottom: 0; }
    .footer .col:nth-child(2), .footer .col:nth-child(3),.footer .col:nth-child(4),.footer .col:nth-child(5) { width: 100%; float: none; padding-left: 0 }
    .footer .col:nth-child(5) { text-align: center;}
}
@media (max-width: 767px) { 
    #home .btn { width: 198px; display: block; margin: 0 auto 20px }
    #home .titrePage { font: 50px/54px dinpl;  margin-bottom: 50px; }
    .primary .wrapper { max-width: 100%; margin: 0 20px }
    #home img { width: 200px; }
    .caption form>div:nth-child(1) { width: 47.2%  }
    .caption form>div:nth-child(2) { width: 47.2%  }
    .caption form>div:nth-child(3) { width: 47.2% }

    #home .titrePage { font: 27px/35px dinpl; }
    #home .titrePage span { font: 18px/24px popl; }
    .primary { padding: 0;}
    .primary .wrapper { max-width: 100%; margin: 0; padding: 0; }
    .primary .wrap { padding: 50px 20px;}
    .primary .wrap>div { padding: 0;}
    .fixe, .height { min-height: inherit; }
    .slide-bg, .slide-area { display: none !important;}
    .absolu { height: inherit; position: relative;}

    .height { padding-top: 250px !important; background-position-y: -377px !important; background-attachment: inherit;}
    .arrow { display: none;}
    .langue { top: 20px; left: 20px }
    .logo-menu { margin: 0 }
    .titrePage { font: 27px/35px dinpl; }
    .titre { font: 27px/35px dinpl;margin-bottom: 25px; }
    .titrePage span:before, .titrePage span:after { display: none; }
    .openToogle, .closeToogle { top: 20px; right: 20px }
    .logo-menu img { width: 150px !important; margin:40px 0 20px; }
		.titrePage 		{  padding:15px 15px 10px; }

}
@media (max-width: 600px) {
    body { line-height: 24px }
    .menu { width: 100%; right: -100% }
    .contactRight>div { margin: 0 20px; }
    .logo { width: 120px; top: 20px; }
    .cocktail-bg, .dine-bg { height: 400px;}
    #stay .caption                 { padding: 8em 20px }
    .location .primary             { padding: 135px 0 244px;}
    .caption form>div              { margin: 0 }
    .caption form>div:nth-child(1) { width: 100%;  }
    .caption form>div:nth-child(2) { width: 100%;  }
    .caption form>div:nth-child(3) { width: 100%; margin: 0 0 15px 0 }
    form .form-control             { height: 45px; }
    form textarea.form-control     { height: auto; }
    form label.error               { padding: 12.5px 20px 0; margin-top: 24px; background-color: #d9534f; }
    .form-wrapper .msg             { top: 0 }

    #banner { height: 400px; position: relative;}
    .scrollBot { display: none; }
    .openToogle { text-indent: -1000px; }
    #footer .wrapper { width: 90%;}
}
@media (max-width: 400px) {
    #footer .wrapper { width: 87%;}
}

/* SLICK */
.slick-slider { position:relative; display:block; -moz-box-sizing:border-box; box-sizing:border-box; -webkit-user-select:none; -moz-user-select:none; -ms-user-select:none; user-select:none; -webkit-touch-callout:none; -khtml-user-select:none; -ms-touch-action:pan-y; touch-action:pan-y; -webkit-tap-highlight-color:transparent;}
.slick-list	{ position:relative; display:block; overflow:hidden; margin:0; padding:0;}
.slick-list:focus { outline:none;}
.slick-list.dragging { cursor:pointer; cursor:hand;}
.slick-slider .slick-track, .slick-slider .slick-list { -webkit-transform:translate3d(0 0,0); -moz-transform:translate3d(0,0,0); -ms-transform:translate3d(0,0,0); -o-transform:translate3d(0,0,0); transform:translate3d(0,0,0);}
.slick-prev { width:50px; height:70px; background:url(images/icone-arrow-left.svg) 46% 50% no-repeat #fff; position:absolute; left:0; top:50%; z-index:50; cursor:pointer; text-indent:-9999px; outline:none; border:0; padding:0; margin-top:-35px;}
.slick-next { width:50px; height:70px; background:url(images/icone-arrow-right.svg) 56% 50% no-repeat #fff; position:absolute; right:0; top:50%; z-index:50; cursor:pointer; text-indent:-9999px; outline:none; border:0; padding:0; margin-top:-35px;}
.slick-dots	{ width:100%; height:10px; margin-top:-37px; position:relative; text-align:center; padding:0; line-height:0; z-index:50;}
.slick-dots	li { display:inline-block; height:10px; margin:0 10px;}
.slick-dots button { background:#b2b2b2; display:block; width:10px; height:10px; border-radius:50%; text-indent:-9999px;outline:none; padding:0;border:1px solid #b2b2b2; cursor:pointer;}
.slick-active button{ background:#373a8c; border:1px solid #373a8c;}
.slick-track{ position:relative; top:0; left:0; display:block;}
.slick-track:before,.slick-track:after { display:table; content: '';}
.slick-track:after{ clear:both;}
.slick-loading .slick-track	{ visibility:hidden;}
.slick-slide{ display:none; float:left; height:100%; min-height:1px;}
[dir='rtl'] .slick-slide{ float:right;}
.slick-slide img{ display:block;}
.slick-slide.slick-loading img{ display:none;}
.slick-slide.dragging img{ pointer-events:none;}
.slick-initialized .slick-slide{ display:block;}
.slick-loading .slick-slide	{ visibility:hidden;}
.slick-vertical .slick-slide { display:block; height:auto; border:1px solid transparent;}
</style>
<?php
	if($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["contact"] == 1){
		
		require ('./phpmailer/PHPMailerAutoload.php');
		$sendTo = 'eric@maki-agency.com';
		$headers = "MIME-Version: 1.0" . "\r\n";
	    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	    $msg = 0;
	    $name = !empty($_POST["name"]) ? strip_tags($_POST["name"]) : '';
	    $email = !empty($_POST["email"]) ? strip_tags($_POST["email"]) : '';
	    $comments = !empty($_POST["comments"]) ? strip_tags($_POST["comments"]) : '';
	    if(!empty($name) && !empty($email) && !empty($comments)){
	    	ob_start();
        	include("email/email_contact.php");
		    $message = ob_get_clean();
		    $mail   = new PHPMailer();  
		    $mail->CharSet  = 'UTF-8'; 
		    $mail->From = $email;  
		    $mail->FromName = $name;  
		    $mail->setFrom    ($email, $name);
		    $mail->AddReplyTo ($email, $name);
		    $mail->Subject    = "Contact";
		    $mail->MsgHTML($message);
		    $mail->addAddress ($sendTo);
            if($mail->send()){
                $msg = 1;
            }
            exit();
	    }

	    echo $msg;

	}else{
		die('Disallow access');
	}
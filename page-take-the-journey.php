<?php $namePage="pageJourney"; $lang ="en"; ?>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Take the Journey - The Litchi Tree</title>
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<![endif]-->

<?php include "css/css.php";?>
<?php include "css/animate.php";?>
</head>

<body>
    <div id="page">
        <?php include "header.php";?>
        <div id="contentPage">
        	<article class="intro parallax">
	        	<div class="inner_parallax">
                    <div class="wrapper">
                        <div class="col2 wow fadeInLeft" >
                            <div class="photoPage">
                                <img src="images/photo-1.jpg" alt="">
                            </div>
                        </div>
                        <div class="col2 wow fadeInRight">
                            <div class="titre">
                                <span>Our history</span>Circa 1902
                            </div>
                            <p>The Litchi Tree, boutique hotel, is located on the heights of Joffreville at the foot of the Amber Mountain National Park.
                            At 45 minutes from the airport of Diego Suarez, come and enjoy a fine moment of calm and serenity, in this old 1902 colonial house that was once the residence of Marshal Joffre and the french admiralty. </p>
                        </div>
                        <div class="clear"></div>
                    </div>      
                </div>
        	</article>
        	<article class="primary fixe parallax">
        	    <div class="inner_parallax">
                    <div class="slide-bg">
                        <div class="banner1"></div>
                        <div class="banner2"></div>
                        <div class="banner3"></div>
                        <div class="banner4"></div>
                    </div>
                    <div class="absolu">
                        <div class="wrapper wow fadeInUp display">
                            <div class="wrap">
                                <div class="mask">
                                    <div class="titre">
                                        <span>A fine moment of calm</span>Come and enjoy
                                    </div>
                                    <p>The property of 5 hectares offers a lot of litchi and mango trees. In the garden you can spot chameleons, leaf tailed geckos, boas and other snakes.    Let yourself be surprised by the gentle melody of the nature and contemplate the view over the Diego Suarez bay and the Mozambique Channel from our main terrace</p>
                                </div>
                                <div class="arrow"></div>
                            </div>
                        </div>
                    </div>
                </div>
        	</article>
        	<article class="secondary parallax">
	        	<div class="inner_parallax">
                    <div class="wrapper wrapper2">
                        <div class="col2 wow fadeInLeft">
                            <div class="titre">
                                <span>Witness of</span>your well being
                            </div>
                            <p>Witness of your well-being, The Litchi Tree offers you six rooms where the nobles wood of Madagascar alongside a rich decoration in a harmonious combination of contemporary and traditional design. All the large rooms and en-suit bathrooms are equipped with solar heated water in order to respect our environment. Water comes from the Amber Mountain waterfall and as it’s a precious and decreasing resource, we thank you to enjoy it respectfully during your stay.</p>
                        </div>
                        <div class="col2 wow fadeInLeft">
                            <div class="slide-img">
                                <div class="photoPage">
                                    <img src="images/photo-2.jpg" alt="">
                                </div>
                                <div class="photoPage">
                                    <img src="images/photo-3.jpg" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
        	</article>
        </div>
        <?php include "footer.php";?>
    </div>
</body>
</html>